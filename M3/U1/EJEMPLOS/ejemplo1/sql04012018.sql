﻿-- empleados cuyo oficio sea analista

SELECT * FROM emple WHERE oficio='analista';

  -- empleados cuyo oficio sea analista y ganen mas de 2000;

    SELECT * FROM emple WHERE oficio='analista' AND salario>2000;

   -- listar apellido de todos los empleados Y ordenarlos por oficio Y por apellido;

    SELECT apellido FROM emple ORDER BY oficio,apellido;
   
-- 36) Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por Z. Listar todos los campos de la tabla empleados

  SELECT * FROM emple WHERE apellido  NOT  LIKE '%z';

  -- código de los empleados cuyo salario sea mayor de 2000

    SELECT emp_no FROM emple WHERE salario>2000;

    -- (15) mostrar los códigos y apellidos de los empleados cuyo salario sea menos que 2000

      SELECT emp_no, apellido FROM emple WHERE salario<2000;

-- mostrar los datos de los empleados cuyo salario este entre 1500 y 2500

    
  SELECT * FROM emple WHERE salario BETWEEN 1500 AND 2500;

  -- seleccionar el apellido y el oficio de los empleados del depart. n. 20


   
 SELECT apellido, oficio FROM emple WHERE dept_no=20;

-- 	(21) Mostrar todos los datos de empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente.

0