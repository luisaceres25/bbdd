﻿DROP DATABASE IF EXISTS ejercicio2;

CREATE DATABASE IF NOT EXISTS ejercicio2;
USE ejercicio2;

CREATE TABLE socio (
  dni char(10) PRIMARY KEY,
  nombre varchar(20),
  direccion varchar(20),
  telefono varchar(20),
  fecha date
 );
INSERT INTO socio VALUES ('7896655427','ramiro','mi casa','897897897','2019/01/23');
-- SELECT * FROM socio;

CREATE TABLE amarre(
  n_amarre char(10) PRIMARY KEY,
  contluz varchar(20),
  contagua varchar(20),
  serviciomant varchar(20)
  );
INSERT INTO amarre VALUES ('am1','12','13','47');
INSERT INTO amarre VALUES ('am2','12','13','47');
INSERT INTO amarre VALUES ('am2888888888888888888','12','13','47');
-- SELECT * FROM amarre;

CREATE TABLE embarcacion(

  matricula char(10) PRIMARY KEY,
  nombre varchar(20),
  tipo varchar(20),
  dimensiones varchar(20),
  amarres char(10) UNIQUE KEY,
  CONSTRAINT  FKembarcacionAmarres FOREIGN KEY (amarres) REFERENCES amarre(n_amarre) on DELETE CASCADE ON UPDATE CASCADE);

INSERT INTO embarcacion VALUES ('171719','prima','a','10','am28888888');
-- SELECT * FROM embarcacion;

CREATE TABLE tienen(
  socio char(10),
  embarcacion char(10),
  PRIMARY KEY(socio,embarcacion),
  CONSTRAINT  FKtienensocio FOREIGN KEY (socio) REFERENCES socio(dni) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKtienenembarcacion FOREIGN KEY (embarcacion) REFERENCES embarcacion(matricula) on DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO tienen VALUES ('7896655427','171719');
-- SELECT * FROM tienen;


CREATE TABLE compra(
  socio char(10),
  amarre char(10) UNIQUE KEY,
  fecha date,
  PRIMARY KEY(socio,amarre),
  CONSTRAINT  FKcomprasocio FOREIGN KEY (socio) REFERENCES socio(dni) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKcompraamarre FOREIGN KEY (amarre) REFERENCES amarre(n_amarre) on DELETE CASCADE ON UPDATE CASCADE);

INSERT INTO compra VALUES ('7896655427','am1','1963/11/12');
-- SELECT * FROM compra;

CREATE TABLE zona(
  letra char(10) PRIMARY KEY,
  tipobarco varchar(20),
  numbarco int,
  profundidad float,
  anchoamarre float);

INSERT INTO zona VALUES ('3','grande',10,23.12,87.13);
-- SELECT * FROM zona;


CREATE TABLE empleado(

  codigo char(10) PRIMARY KEY,
  nombre varchar(20),
  direccion varchar(20),
  telefono varchar(20),
  especialidad varchar(20));


INSERT INTO empleado VALUES ('34','imanol','santander', '26262626', 'electronica');
INSERT INTO empleado VALUES ('35','imanol','santander', '26262626', 'electronica');
INSERT INTO empleado VALUES ('36','imanol','santander', '26262626', 'electronica');
-- SELECT * FROM empleado;


CREATE TABLE asigna(
  zona char(10),
  empleado char(10),
  numbarcos int,
   PRIMARY KEY(zona,empleado),
  CONSTRAINT  FKasignazona FOREIGN KEY (zona) REFERENCES zona(letra) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKasignaempleado FOREIGN KEY (empleado) REFERENCES empleado(codigo) on DELETE CASCADE ON UPDATE CASCADE);

INSERT INTO asigna VALUES ('3','35',10);
--  SELECT * FROM asigna;


CREATE TABLE pertenece(
  amarre char(10)UNIQUE KEY,
  zona char(10),
  PRIMARY KEY(amarre,zona),
  CONSTRAINT  FKperteneceamarre FOREIGN KEY (amarre) REFERENCES amarre(n_amarre) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKpertenecezona FOREIGN KEY (zona) REFERENCES zona(letra) on DELETE CASCADE ON UPDATE CASCADE);

INSERT INTO pertenece VALUES ('am1','3');
--  SELECT * FROM pertenece;







