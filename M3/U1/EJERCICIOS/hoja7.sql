﻿/* 
  hoja7 - unidad 1 - módulo 3
*/
  -- eliminar la base de datos si existe
  DROP DATABASE IF EXISTS hoja7unidad1modulo3;

  --  crear la base de datos si no existe
  CREATE DATABASE hoja7unidad1modulo3;
 -- utilizar la base de datos    
  USE hoja7unidad1modulo3;
   DROP TABLE IF EXISTS empleado;


  CREATE TABLE empleado (
        dni char(9),
        PRIMARY KEY (dni)
          
    );
INSERT INTO empleado VALUES 
  ('dni1'),
  ('dni2');
  
SELECT e.dni FROM hoja7unidad1modulo3.empleado e;


DROP TABLE IF EXISTS departamento;

CREATE TABLE departamento (
 codDpto char(15),
  PRIMARY KEY(codDpto)
  );

insert INTO departamento VALUES
  ('departamento1'),
  ('departamento2');



   -- eliminar la tabla
    DROP TABLE IF EXISTS pertenece;

-- crear la tabla pertenece
  CREATE TABLE IF NOT EXISTS pertenece(
    departamento varchar(15),
    empleado char (9),
    PRIMARY KEY (departamento,empleado),
    CONSTRAINT uniqueEmpleado UNIQUE KEY (empleado),
    CONSTRAINT FKperteneceEmpleado FOREIGN KEY (empleado)
     REFERENCES empleado(dni) on DELETE CASCADE on UPDATE CASCADE,
    CONSTRAINT FKPerteneceDepartamento FOREIGN KEY (departamento)
     REFERENCES departamento(codDpto) on DELETE CASCADE on UPDATE CASCADE
    );

  DROP TABLE IF EXISTS proyecto;
  -- crear la tabla proyecto

    CREATE TABLE IF NOT EXISTS proyecto(
      codigoProyecto char(15),
      PRIMARY KEY (codigoProyecto)
      );
-- eliminar la tabla 
  DROP TABLE IF EXISTS trabaja;
-- crear tabla trabaja
CREATE TABLE IF NOT EXISTS trabaja(
  empleado char(9),
  proyecto char(15),
  fecha date,
  PRIMARY KEY (empleado,proyecto),
  CONSTRAINT FKTrabajaEmpleado FOREIGN KEY(empleado)
      REFERENCES empleado(dni)
      on DELETE CASCADE on UPDATE CASCADE,
  CONSTRAINT FkTrabajaProyecto FOREIGN KEY(proyecto)
      REFERENCES proyecto(codigoProyecto)
      on DELETE CASCADE on UPDATE CASCADE
  );



       


