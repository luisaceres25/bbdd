﻿-- hoja 7 ejercicio 2

    DROP DATABASE IF EXISTS hoja7unidad1Modulo3Ejercicio2;
    CREATE DATABASE IF NOT EXISTS hoja7unidad1Modulo3Ejercicio2;
    USE hoja7unidad1Modulo3Ejercicio2;
    
    
-- crear la tabla compañia

  DROP TABLE IF EXISTS compania;
  CREATE TABLE IF NOT EXISTS compania(  
  numero char(9),
  actividad char(15),
    PRIMARY KEY (numero) 
  );

  -- crear la tabla soldado

    DROP TABLE IF EXISTS soldado;
    CREATE TABLE IF NOT EXISTS soldado(
    s char(15),
    nombre char(15),
    apellidos char(15),
    grado char(15),
      PRIMARY KEY (s)
    
    );


      -- crear la tabla pertenece

    DROP TABLE IF EXISTS pertenece;
    CREATE TABLE IF NOT EXISTS pertenece (
      compania char(15),
      soldado char(15),
      PRIMARY KEY (compania,soldado),
      CONSTRAINT FKperteneceCompania FOREIGN KEY(compania)
        REFERENCES compania(numero)
        on DELETE CASCADE on UPDATE CASCADE,
      constraint FKperteneceSoldado FOREIGN KEY(soldado)
        REFERENCES soldado(s)
      ON DELETE CASCADE ON UPDATE CASCADE    
    );

    -- crear la tabla servicio

      DROP TABLE IF EXISTS servicio;
      CREATE TABLE IF NOT EXISTS servicio(
      se char(15),
      descripcion char(15),
        PRIMARY KEY(se)              
      );
    
     -- crear la tabla cuerpo
      DROP DATABASE IF EXISTS cuerpo;
      CREATE TABLE IF NOT EXISTS cuerpo(
      c char(15),
      demo char(15),
       PRIMARY KEY(c)
      );

      -- crear la tabla cuartel

      DROP DATABASE IF EXISTS cuartel;
      CREATE TABLE IF NOT EXISTS cuartel(
      cu char(15),
      nombre char(15),
      dir char(15),
      PRIMARY KEY(cu)
      );
      -- crear la tabla realiza

        DROP DATABASE IF EXISTS realiza;
        CREATE TABLE IF NOT EXISTS realiza(
          s char(15),
        se char(15),
          PRIMARY KEY(s,se),
          CONSTRAINT FKrealizaSoldado FOREIGN KEY(s)
          REFERENCES soldado(s)
        on DELETE CASCADE on UPDATE CASCADE,
        CONSTRAINT FKrealizaServicio FOREIGN KEY(se)
          REFERENCES servicio(se)
          on DELETE CASCADE on UPDATE CASCADE         
        );
      -- crear la tabla esta
        DROP DATABASE IF EXISTS esta;
        CREATE TABLE IF NOT EXISTS esta(
          s char(15),
          cu char(15),
          PRIMARY KEY(s,cu),
          CONSTRAINT FKestaSoldado FOREIGN KEY(s)
          REFERENCES soldado(s)
          on DELETE CASCADE on UPDATE CASCADE,
           CONSTRAINT FKestaCuartel FOREIGN KEY(cu)
          REFERENCES cuartel(cu)
          on DELETE CASCADE on UPDATE CASCADE
          );
         -- crear la tabla pertenece
          DROP DATABASE IF EXISTS perteneceCuerpo;
          CREATE TABLE IF NOT EXISTS perteneceCuerpo(
          s char(15),
          c char(15),
          PRIMARY KEY(s,c),
          CONSTRAINT FKperteneceCuerpoSoldado FOREIGN KEY(s)
          REFERENCES soldado(s)
          ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT FKperteneceCuerpoCuerpo FOREIGN KEY(c)
          REFERENCES cuerpo(c)
          ON DELETE CASCADE ON UPDATE CASCADE

          );