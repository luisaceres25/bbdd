﻿-- Consultas de selección 5
--  1.-Nombre y edad de los ciclistas que no han ganado etapas
  SELECT distinct nombre,edad FROM ciclista c LEFT JOIN etapa e USING (dorsal) WHERE e.dorsal IS NULL;

  CREATE OR REPLACE VIEW consulta6c1 AS
     SELECT distinct nombre,edad FROM ciclista c LEFT JOIN etapa e USING (dorsal) WHERE e.dorsal IS NULL;

-- 2.-Nombre y edad de los ciclistas que no han ganado puertos

  SELECT distinct nombre,edad FROM ciclista c LEFT JOIN puerto p USING (dorsal) WHERE p.dorsal IS NULL;

-- 3 listar el director de los equipos que tengan ciclistas que no hayan ganado nincuna etapa

              -- primero listar ciclistas que no han ganado ninguna etapa  

                SELECT DISTINCT c.nomequipo FROM ciclista c LEFT JOIN etapa e USING (dorsal) WHERE e.dorsal IS NULL;
                
                SELECT e.director FROM equipo e JOIN (SELECT DISTINCT c.nomequipo FROM ciclista c LEFT JOIN etapa e USING (dorsal) WHERE e.dorsal IS NULL) AS c1
                 USING(nomequipo);

-- 4.dorsal y nombre de los ciclistas que no hayan llevado ningún maillot
              SELECT * FROM ciclista c LEFT JOIN lleva l USING(dorsal) WHERE l.dorsal IS NULL;
            
              SELECT c.dorsal, c.nombre FROM ciclista c JOIN(  SELECT * FROM ciclista c LEFT JOIN lleva l USING(dorsal) WHERE l.dorsal IS NULL) AS c2 USING(dorsal);


-- 5.dorsal y nombre de los ciclistas que no hayan llevado el maillot amarillo nunca

     SELECT distinct dorsal FROM lleva WHERE código='MGE';

     SELECT c.dorsal,c.nombre FROM ciclista c left JOIN (  SELECT distinct dorsal FROM lleva WHERE código='MGE') AS c1
     USING (dorsal) WHERE c1.dorsal IS NULL;
