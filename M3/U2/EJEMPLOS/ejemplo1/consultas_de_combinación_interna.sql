﻿-- listar las edades de todos los ciclistas de Banesto

  SELECT edad FROM ciclista WHERE nomequipo='Banesto';

-- listar el dorsal de los ciclistas que son de Kelme y cuya edad esta entre 25 y 32

  SELECT dorsal FROM ciclista WHERE nomequipo='Kelme' AND edad BETWEEN 25 AND 32;

  --pregunta 8 indicarme el código de etapa cuya altura media de sus puertos está por encima de 1500

  SELECT numetapa FROM puerto GROUP BY numetapa HAVING AVG(altura)>1500;

  -- pregunta 9 indicarme nombre y edad de los ciclistas que han ganado etapas

    SELECT distinct nombre, edad FROM ciclista JOIN etapa on ciclista.dorsal=etapa.dorsal;

-- pregunta 10 indicarme nombre y edad de los ciclistas que han ganado puertos

    SELECT distinct nombre, edad FROM ciclista JOIN puerto on ciclista.dorsal=puerto.dorsal;

-- pregunta 6 dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo

  SELECT distinct ciclista.dorsal,ciclista.nombre 
FROM ciclista JOIN lleva ON ciclista.dorsal=lleva.dorsal JOIN maillot ON lleva.código=maillot.código WHERE color='amarillo';


  -- pregunta 7 dorsal de los ciclistas que hayan llevado algún maillot y que hayan ganado etapas;

    SELECT distinct lleva.dorsal FROM lleva JOIN etapa ON lleva.numetapa=etapa.numetapa;


-- pregunta 8 indicar el numetapa de las etapas que tengan puerto

  SELECT DISTINCT puerto.numetapa FROM etapa JOIN puerto ON puerto.numetapa=etapa.numetapa;

-- pregunta 9 indicar los km de las etapas que hayan ganado ciclistas de Banesto y que tengan puertos

  SELECT distinct etapa.kms 
  FROM etapa JOIN ciclista ON etapa.dorsal=ciclista.dorsal  JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE nomequipo='Banesto';

