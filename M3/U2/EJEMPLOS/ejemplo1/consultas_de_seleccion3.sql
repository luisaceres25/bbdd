﻿/* Consultas de selección 3 */

  -- listar las edades de todos los ciclistas de Banesto

  SELECT distinct edad FROM ciclista WHERE nomequipo='Banesto';



  -- listar las edades de ciclistas que son de Banesto o de Navigare

    SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Banesto' OR nomequipo='Navigare';

-- listar el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32

  SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 25 AND 32;

  -- listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32

    SELECT * FROM ciclista WHERE nomequipo='Banesto' OR edad BETWEEN 25 AND 32;

-- listar la inicial de los ciclistas cuya inicial empiece por R

  SELECT DISTINCT LEFT (nomequipo,1) FROM ciclista WHERE nombre LIKE 'r%';

  -- listar el código de las etapas cuya salida y llegada sea en la misma población(6)

SELECT numetapa FROM etapa WHERE salida=llegada;


-- listar el código de las etapas que su salida y llegada no sean en la misma población y que conozcamos el dorsal del ciclista que ha ganado la etapa


SELECT numetapa FROM etapa WHERE salida<>llegada AND dorsal IS NOT NULL;

-- -- listar el nombre de los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400

  SELECT * FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400;


-- listar el dorsal de los ciclistas que hayan ganado puertos cuya altura entre 1000 y 2000 o cuya altura sea mayor que 2400

  SELECT dorsal FROM puerto WHERE altura BETWEEN 1000 AND 2000 OR altura>2400;



-- listar el numero de ciclistas que hayan ganado alguna etapa;

SELECT COUNT(DISTINCT dorsal) FROM etapa;


-- listar el numero de etapas que tengan puerto(11)

  SELECT COUNT(DISTINCT numetapa) FROM puerto;

-- listar el numero de ciclistas que hayan ganado algún puerto

  SELECT COUNT(DISTINCT dorsal) AS nCiclistas FROM puerto;

  -- listar el código de la etapa con el numero de puertos que tiene

    SELECT COUNT(*), numetapa AS nPuertos FROM puerto GROUP BY numetapa;

-- indicar la altura media de los puertos.

  SELECT AVG(altura) FROM puerto;

  -- indicar el codigo de etapa cuya altura media de sus puertos esta por encima de 1500

    SELECT numetapa FROM puerto GROUP BY numetapa HAVING AVG(altura)>1500;

-- indicar el numero de etapas que cumplen la condicion anterior



SELECT COUNT(*) AS nPuertos FROM  (SELECT numetapa FROM puerto GROUP BY numetapa HAVING AVG(altura)>1500) as c1;

-- listar el dorsal del ciclista con el numero de veces que ha llevado algún maillot(17)

  SELECT dorsal,count(*) AS nMaillots FROM lleva GROUP BY dorsal;

  -- listar el dorsal del ciclista con el código de maillot y cuantas veces el ciclista ha llevado algún maillot

    SELECT dorsal,código,COUNT(*) AS numero FROM lleva GROUP BY dorsal,código;


-- listar el dorsal, el código de etapa, el ciclista y el numero de maillots que ese ciclista ha llevado en cada etapa

 
 SELECT dorsal,numetapa,COUNT(*) AS numero FROM lleva GROUP BY numetapa,dorsal;











