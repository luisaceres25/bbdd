﻿-- mostrar los campos y los registros de la tabla empleado.

  SELECT * FROM emple;


  -- mostrar los campos y los registros de la tabla departamento.

SELECT * FROM depart;

-- mostrar numero, nombre y localizacion de la cada departamento.

  SELECT * FROM depart;

  -- datos de empleados ordenados por numero de departamento descendentement.

    SELECT * FROM emple ORDER BY dept_no DESC;

-- datos de empleados ordenados por numero de departamento descendentemente y por oficio ascendentemente.

  SELECT * FROM emple ORDER BY dept_no DESC,oficio;

  -- datos de empleados ordenados por numero de departamento descendentemente y por apellido ascendentemente.

    SELECT * FROM emple ORDER BY dept_no DESC,apellido;

    -- mostrar el apellido y oficio de cada empleado;

      SELECT apellido,oficio FROM emple; 

  -- mostrar localizacion y numero de cada departamento.

    SELECT loc,dept_no FROM depart;

-- datos de empleados ordenados por apellido de forma ascendente;

  SELECT * FROM emple ORDER BY apellido ASC;

  -- datos de empleados ordenados por apellido de forma descendiente;

SELECT * FROM emple ORDER BY apellido DESC;
    


 



