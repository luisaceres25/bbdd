﻿

USE practica1; -- selecciono la base de datos con la que voy a trabajar;

/* 
  CONSULTA 1
  */

SELECT  e.emp_no,
        e.apellido,
        e.oficio,
        e.dir,
        e.fecha_alt,
        e.salario,
        e.comision,
        e.dept_no FROM emple e;
