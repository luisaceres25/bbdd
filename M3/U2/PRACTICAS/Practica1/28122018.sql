﻿-- que provincias estan en autonomías de nombre compuesto

  SELECT provincia FROM provincias WHERE autonomia LIKE '% %';

 -- que provincias tienen nombre compuesto

  SELECT provincia FROM provincias WHERE provincia LIKE '% %';

  -- cuantos caracteres tiene cada comunidad autonoma y ordena el resultado de autonomias de forma descendente


SELECT DISTinct autonomia,CHAR_LENGTH(autonomia)  FROM provincias ORDER BY autonomia DESC;

-- que provincias tienen nombre simple

SELECT provincia FROM provincias WHERE provincia NOT LIKE '% %';

-- autonomías de nombre compuesto y ordenarlas alfabeticamente en orden inverso


  SELECT DISTINCT autonomia FROM provincias WHERE autonomia LIKE '% %' ORDER BY autonomia DESC;

-- autonomías que comiencen por 'can' ordenadas alfabeticamente

  SELECT DISTINCT autonomia from provincias WHERE autonomia LIKE 'can%' ORDER BY autonomia;

-- autonomías con provincias de mas de 1 millon de hab y ordenar alfabeticamente.

SELECT DISTINCT autonomia FROM provincias WHERE poblacion>1e6 ORDER BY autonomia;


-- poblacion del pais

  SELECT SUM(poblacion) FROM provincias;

  -- cuantas provincias hay en la tabla

SELECT COUNT(provincia) FROM provincias;

-- comunidades que contienen nombre de una de sus provincias

SELECT DISTINCT autonomia FROM provincias WHERE autonomia like CONCAT('%', provincia,'%');


-- 13) ¿Qué autonomías tienen nombre simple? Ordena el resultado alfabéticamente en orden inverso


  SELECT DISTINCT autonomia FROM provincias WHERE autonomia NOT LIKE '% %' ORDER BY autonomia desc;



  -- 14) ¿Qué autonomías tienen provincias con nombre compuesto? Ordenar el resultado alfabéticamente



    SELECT DISTINCT autonomia FROM provincias WHERE provincia LIKE '% %' ORDER BY autonomia;

    -- superficie del pais

      SELECT SUM(superficie) FROM provincias;

--  En un listado alfabético, ¿qué provincia estaría la primera?

  SELECT Min(provincia) FROM provincias; 


  --  ¿Qué provincias tienen un nombre más largo que el de su autonomía?


    SELECT provincia FROM provincias WHERE CHAR_LENGTH(provincia)>CHAR_LENGTH(autonomia);

    -- cuantas comunidades autonomas hay;

      SELECT  COUNT(DISTINCT autonomia) FROM provincias;


 --Cuánto mide el nombre de autonomía más corto
  

      SELECT Min( CHAR_LENGTH(autonomia)) FROM provincias;

--Cuánto mide el nombre de provincia más largo?

SELECT MAX(CHAR_LENGTH(provincia)) FROM provincias;

--Población media de las provincias entre 2 y 3 millones de habitantes sin decimales

SELECT round( AVG(poblacion )) FROM provincias WHERE poblacion BETWEEN 2e6 AND 3e6;