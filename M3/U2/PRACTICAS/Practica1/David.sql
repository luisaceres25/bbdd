﻿SELECT 2*4;


SELECT provincia FROM provincias;


SELECT provincia,poblacion/superficie densidad FROM provincias;

SELECT SQRT(2) raiz_cuadrada;

SELECT provincia,CHAR_LENGTH(provincia) FROM provincias;

-- provincias con mismo nombre autonomia

SELECT provincia FROM provincias
  WHERE provincia=autonomia;

-- listado de provincias

  SELECT provincia FROM provincias;

  -- caracteres de cada provincia

    SELECT provincia,CHAR_LENGTH(provincia) FROM provincias;

-- listado de autonomías

  SELECT DISTINCT autonomia FROM provincias;

-- provincias que contienen el diptongo 'ue'

  SELECT provincia FROM provincias WHERE provincia LIKE '%ue%';
-- provincias que empiecen por 'a'

  SELECT provincia FROM provincias WHERE provincia LIKE 'a%';


  -- autonomias terminadas en 'ana'

    SELECT DISTINCT autonomia FROM provincias WHERE autonomia LIKE '%ana';
