﻿-- (34) Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A. Listar el apellido de los empleados

  SELECT apellido  FROM emple WHERE SUBSTR(apellido,1,1)='A' ;
-- (6) Indicar el número de empleados que hay

  SELECT COUNT(*) FROM emple;


-- (9) Indicar el numero de departamentos que hay

  SELECT COUNT(*) FROM depart;

  -- (20) Contar el número de empleados cuyo oficio sea VENDEDOR

    SELECT count(*) FROM emple WHERE oficio='vendedor';
-- (29) Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece

  SELECT apellido,dnombre FROM emple JOIN depart USING(dept_no);
/* (30)Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertenece.
  Ordenar los resultados por apellido de forma descendente.*/

  SELECT apellido,oficio,dnombre FROM emple JOIN depart USING(dept_no) ORDER BY apellido DESC;

-- (23) Mostrar los apellidos del empleado que más gana

  SELECT MAX(salario) FROM emple AS  c1;
  SELECT apellido FROM emple WHERE salario=(SELECT MAX(salario) FROM emple);

-- (10) Indicar el número de empleados más el número de departamentos





