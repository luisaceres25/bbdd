﻿
/* Practica numero 2 */

  -- Consultas de la practica 2

-- 1.Indicar el numero de ciudades que hay en la tabla ciudades


  SELECT COUNT(*)  AS CUENTA_CIUDADES FROM ciudad;

-- 2.Indicar el nombre de las ciudades que tengan una poblacion por encima de la poblacion media
   
SELECT AVG(población)  AS POBLACION_MEDIA FROM ciudad;

SELECT NOMBRE from ciudad WHERE población>(SELECT AVG(población)  AS POBLACION_MEDIA FROM ciudad);

--  3. Indicar el nombre de las ciudades que tengan una poblacion media por debajo de la poblacion media

  SELECT nombre FROM ciudad WHERE poblacion<(SELECT AVG(población) AS POBLACION_MEDIA FROM ciudad);

-- 6.Indicar el numero de ciudades que tengan una poblacion por encima de la poblacion media

  SELECT COUNT(*) AS Mayor_que_media FROM ciudad WHERE población>(SELECT AVG(población)  AS POBLACION_MEDIA FROM ciudad);

-- 7. Indicarme el numero de personas que viven en cada ciudad

  SELECT COUNT(*),ciudad FROM persona GROUP BY ciudad;

-- 8. Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañías

  SELECT COUNT(*),compañia FROM trabaja GROUP BY compañia;

-- 9. Indicarme la compañia que mas trabajadores tiene

  SELECT COUNT(*),compañia FROM trabaja GROUP BY compañia;

  SELECT c1.compañia FROM (SELECT COUNT(*) cuenta,compañia FROM trabaja GROUP BY compañia) c1 
  WHERE c1.cuenta =( SELECT MAX(c2.cuenta) FROM (SELECT t.compañia,COUNT(*) AS cuenta FROM trabaja t GROUP BY(t.compañia)) AS c2);

    SELECT compañia, COUNT(persona) AS cuenta FROM trabaja GROUP BY compañia ORDER BY cuenta DESC LIMIT 1;

-- 10. Indicarme el salario medio de cada una de las cias.

   SELECT compañia,AVG(salario) AS SALARIO_MEDIO FROM trabaja GROUP BY compañia;

-- 11. Nombre de las personas y la poblacion de la ciudad donde viven

  SELECT p.nombre AS persona, c.población AS pob FROM ciudad c INNER JOIN persona p ON c.nombre = p.ciudad;

-- 12 Listar el nombre de las personas, la calle y la poblacion de la ciudad donde viven

    SELECT p.nombre AS persona, p.calle AS calle,c.población AS pob FROM ciudad c INNER JOIN persona p ON c.nombre = p.ciudad;

-- 13 Listar el nombre de las personas, la ciudad donde vive y la ciudad donde esta la cia para la que trabaja

    SELECT p.nombre, p.ciudad AS ciudadpersona, c.ciudad AS ciudadcia FROM persona p JOIN trabaja t ON p.nombre = t.persona
      JOIN compañia c ON c.nombre = t.compañia;

-- 14 Realizar el algebre relacional y explicar la siguiente consulta.

    SELECT p.nombre, p.ciudad AS ciudadpersona, c.ciudad AS ciudadcia FROM persona p JOIN trabaja t ON p.nombre = t.persona
      JOIN compañia c ON c.nombre = t.compañia WHERE p.ciudad = c.ciudad ORDER BY p.nombre ASC;

-- 15 Listarme el nombre de la persona y el nombre del supervisor.

  SELECT * FROM supervisa;

-- 16 Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada uno de ellos


  SELECT supervisor,p1.ciudad, persona, p2.ciudad FROM supervisa JOIN persona p1 ON persona = p1.nombre
    JOIN  persona p2 ON supervisor = p2.nombre ORDER BY persona;

-- 17. Indicarme el numero de ciudades distintas que hay en la tabla compañia

  SELECT COUNT(DISTINCT ciudad) FROM compañia;


-- 20 Indicarme el nombre de las personas que no trabajan en Fagor.

  SELECT persona,compañia FROM trabaja WHERE not compañia='Fagor';

-- 23 Listar la población donde vive cada persona, sus salario, su nombre y la cia para la que trabaja.
 --  Ordenar la salida por nombre de la persona y por salario de form descendente

  SELECT c.población,t.salario,p.nombre FROM persona p JOIN trabaja t ON p.nombre = t. persona
    JOIN ciudad c ON p.nombre = t.persona ORDER BY p.nombre,t.salario DESC;














