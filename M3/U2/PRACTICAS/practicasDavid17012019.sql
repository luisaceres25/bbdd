﻿


-- Practicas David 17012019

-- 10 Indicar el número de empleados mas el número de departamentos

(SELECT COUNT(dept_no) FROM depart) AS c1;
(SELECT COUNT(emp_no) FROM emple) AS c2;

SELECT (SELECT COUNT(*) FROM depart) + (SELECT COUNT(*) FROM emple);

-- (31) Listar el número de departamento de aquellos departamentos que tengan  empleados y el número de empleados que tengan.

SELECT   dept_no DEPT_NO, COUNT(*) NUMERO_DE_EMPLEADOS FROM emple GROUP BY dept_no; 

-- 32) Listar el número de empleados por departamento, ordenados de más a menos empleados, incluyendo los departamentos que no tengan empleados.
  -- El encabezado de la tabla será dnombre y NUMERO_DE_EMPLEADOS


  (SELECT dept_no,COUNT(*) FROM emple GROUP BY dept_no) c1;


SELECT depart.dnombre,IFNULL(n,0) n FROM depart LEFT JOIN(
  SELECT dept_no, COUNT(*) n FROM emple GROUP BY dept_no ) c1 USING(dept_no);


