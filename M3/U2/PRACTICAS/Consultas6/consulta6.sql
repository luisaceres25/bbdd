﻿-- Consultas de seleccion 6

  -- 1.1 numero de vendedores cuya fecha de alta sea en febrero de cualquier año

 SELECT COUNT(*) FROM vendedores WHERE MONTH(FechaAlta)=2;

-- 1.2 número de vendedores guapos y feos

  SELECT COUNT(*) FROM vendedores WHERE `Guap@`=TRUE;
  SELECT COUNT(*) FROM vendedores WHERE `Guap@`=FALSE;

-- 1.3 el nombre del producto mas caro

  SELECT MAX(Precio), NomProducto AS productoMasCaro FROM productos;

-- 1.5 indica que grupo tiene producto que se hayan vendido alguna vez

  -- productos que se han vendido alguna vez

 ( SELECT DISTINCT `Cod Producto` FROM ventas) AS c1;

  -- resultado
    SELECT DISTINCT IdGrupo FROM productos INNER JOIN (SELECT DISTINCT `Cod Producto` FROM ventas) c1 ON productos.IdProducto=c1.`Cod Producto`;

  -- 1.4 precio medio de productos por grupo
    SELECT AVG(Precio) FROM productos GROUP BY IdGrupo;

-- 1.6 indica los grupos de los cuales no se ha vendido ningun producto.
  SELECT DISTINCT`Cod Producto` FROM ventas;
  SELECT IdGrupo FROM productos LEFT JOIN ( SELECT DISTINCT`Cod Producto` FROM ventas) c1 ON `Cod Producto`=IdProducto WHERE c1.`Cod Producto` IS NULL ;


-- 1.7 Número de poblaciones cuyos vendedores son guapos.

SELECT COUNT(DISTINCT Poblacion) FROM vendedores WHERE `Guap@`=TRUE;

-- 1.8 Nombre de la poblacion con mas vendedores casados

  SELECT COUNT(*) n, Poblacion FROM vendedores WHERE EstalCivil='Casado' GROUP BY Poblacion; 






